package com.yhc.erp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.just.base.BaseAgentWebActivity
import com.king.zxing.CameraScan
import com.king.zxing.DefaultCameraScan

class MainActivity : BaseAgentWebActivity() {
    companion object{
        private const val TAG = "MainActivity"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        agentWeb.jsInterfaceHolder.addJavaObject("Android", XAndroidInterface(agentWeb, this))
    }

    override fun getUrl(): String? {
        return "http://192.168.3.56:8080/#/"
    }

    override fun getAgentWebParent(): ViewGroup = findViewById(R.id.rlContent)

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == RESULT_OK) {

            var result = data?.getStringExtra("result")
            agentWeb.jsAccessEntrace.quickCallJs("scanResult", result)
            Log.d(TAG, "onActivityResult: $result")
        }
    }
}