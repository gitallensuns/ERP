package com.yhc.erp

import android.app.Activity
import android.content.Intent
import android.webkit.JavascriptInterface
import com.just.agentweb.AgentWeb
import com.just.common.AndroidInterface
import com.king.zxing.CameraConfig
import com.king.zxing.CaptureActivity
import com.king.zxing.util.CodeUtils
import com.king.zxing.DecodeConfig

import com.king.zxing.analyze.MultiFormatAnalyzer


/**
 * @Author 萤火虫
 * @Date 2021/7/23
 * @Desc 根据业务新增与业务相关的交互逻辑
 */
class XAndroidInterface(agent: AgentWeb?, val activity: Activity?) : AndroidInterface(agent, activity) {

    /**
     * 打开扫码功能
     */
    @JavascriptInterface
    fun openScan() {
        activity?.runOnUiThread {
            ScanActivity.start(activity)
        }
    }
}