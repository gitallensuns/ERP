package com.yhc.erp

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.zxing.Result
import com.king.zxing.CaptureActivity
import com.king.zxing.analyze.MultiFormatAnalyzer

import com.king.zxing.DecodeFormatManager

import com.king.zxing.DecodeConfig


class ScanActivity : CaptureActivity() {
    companion object {
        @JvmStatic
        fun start(context: Activity) {
            val starter = Intent(context, ScanActivity::class.java)
            context.startActivityForResult(starter, 1)
        }
    }

    override fun initCameraScan() {
        super.initCameraScan()
        //初始化解码配置
        val decodeConfig = DecodeConfig()
        decodeConfig.setHints(DecodeFormatManager.ALL_HINTS) //如果只有识别二维码的需求，这样设置效率会更高，不设置默认为DecodeFormatManager.DEFAULT_HINTS
            .setFullAreaScan(false) //设置是否全区域识别，默认false
            .setAreaRectRatio(0.8f) //设置识别区域比例，默认0.8，设置的比例最终会在预览区域裁剪基于此比例的一个矩形进行扫码识别
            .setAreaRectVerticalOffset(0).areaRectHorizontalOffset = 0 //设置识别区域水平方向偏移量，默认为0，为0表示居中，可以为负数
        //在启动预览之前，设置分析器，只识别二维码
        cameraScan
            .setVibrate(true) //设置是否震动，默认为false
            .setNeedAutoZoom(true) //二维码太小时可自动缩放，默认为false
            .setAnalyzer(MultiFormatAnalyzer(decodeConfig)) //设置分析器,如果内置实现的一些分析器不满足您的需求，你也可以自定义去实现

    }

    override fun onScanResultCallback(result: Result?): Boolean {
        var intent = Intent().apply {
            this.putExtra("result", result.toString())
        }
        setResult(RESULT_OK, intent)
        finish()
        return super.onScanResultCallback(result)
    }

    override fun onScanResultFailure() {
        super.onScanResultFailure()
    }
}