# #frameweb-module webview -封装类
    
### H5调用常用API
    
 
 
    /**
     *拨打电话 
     */
     window.location.href="tel:10068"
```   
 /**
  *调用原生发送短信 
  * (固定格式)
  */
    window.location.href="sms:10086?body=短信内容"
```

-------

```
/**
*调用原生关闭webview
*/
    webview window.Android.exitWebview()
     
```
-------
```
/**
*调用原生关闭当前页面 
*注意（根据history进行依次返回，当没有历史记录页面时关webview）
*/
    window.Android.finishPagebyHistory()
```
-------

```
/**
 *调用原生浏览器打开页面 
 *注意（可以调用这个方法进行浏览器下载文件）
 *@params url 下载地址或需要打开的地址
 */
    window.Android.openBrowser(String url)
    
    
```
    
-------

``` 
/**
 *调用原生打开高德进行导航 
 *
 * @param context
 * @param slon    起始点-经度
 * @param slat    起始点-维度
 * @param sname   起始点-地址名
 * @param dlon    终点-经度
 * @param dlat    终点-维度
 * @param dname   终点-地址名字
 */
    window.Android.openGaode(String slon, String slat, String sName, String dlon, String dlat, String dName)
    ```

-------
