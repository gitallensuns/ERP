package com.just.common;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.just.agentweb.AgentWeb;

import java.lang.ref.WeakReference;

/**
 * @Author 萤火虫
 * @Date 2021/7/23
 * @Desc
 */
public class AndroidInterface {
    private static final String TAG = "AndroidInterface";
    private WeakReference<AgentWeb> agentWebWeakReference = null;
    private WeakReference<Activity> activityWeakReference = null;

    public AndroidInterface(AgentWeb agent, Activity activity) {
        this.agentWebWeakReference = new WeakReference<>(agent);
        this.activityWeakReference = new WeakReference<>(activity);
    }

    /**
     * 退出整个Webview页面
     */
    @JavascriptInterface
    public void exitWebview() {
        activityWeakReference.get().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                activityWeakReference.get().finish();
            }
        });
    }

    /**
     * 根据webview的历史层级进行返回
     */
    @JavascriptInterface
    public void finishPagebyHistory() {
        Log.d(TAG, "finishPagebyHistory: ");
        activityWeakReference.get().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!agentWebWeakReference.get().back()) {
                    activityWeakReference.get().finish();
                }
            }
        });
    }

    /**
     * 通过浏览器打开地址
     * 可以通过浏览器下载资源文件
     *
     * @param url 打开的地址
     */
    @JavascriptInterface
    public void openBrowser(String url) {
        Log.d(TAG, "openBrowser: " + url);
        if (TextUtils.isEmpty(url) || url.startsWith("file://")) {
            Toast.makeText(activityWeakReference.get(), url + " 该链接无法使用浏览器打开。", Toast.LENGTH_SHORT).show();
            return;
        }
        activityWeakReference.get().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    final Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(url));
                    activityWeakReference.get().startActivity(intent);
                } catch (Exception e) {
                    Log.d(TAG, "openBrowser: " + e.getMessage());
                }
            }
        });
    }

    /**
     * 打开高德导航
     *
     * @param context
     * @param slon    起始点-经度
     * @param slat    起始点-维度
     * @param sname   起始点-地址名
     * @param dlon    终点-经度
     * @param dlat    终点-维度
     * @param dname   终点-地址名字
     */
    @JavascriptInterface
    public void openGaode(String slon, String slat, String sName, String dlon, String dlat, String dName) {
        activityWeakReference.get().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                MapUtil.route(activityWeakReference.get(), slon, dlat, sName, dlon, dlat, dName);
            }
        });
    }


}
